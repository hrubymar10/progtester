<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssertTestcasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assert_testcases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('content');
            $table->integer('user_id');
            $table->integer('task_id');
            $table->integer('approvals_count')->default(0);
            $table->integer('flags_count')->default(0);
            $table->boolean('archive')->default(true);
            $table->boolean('bonus')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assert_testcases');
    }
}
