<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestcaseFlag extends Model
{
    protected $fillable = [
        'testcase_id',
        'user_id',
    ];
}
