<?php

namespace App\Providers;

use App\Helpers\Breadcrumbs;
use App\Service\CvutAuthenticationProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerBladeDirectives();
        $this->registerCvutSocialite();
        $this->forceRootDomain();

        // Needed by older Mysql databases
        Schema::defaultStringLength(191);

        // Used in diffForHumans on homepage
        Carbon::setLocale('cs');
    }

    /**
     * Force the SSL protocol in production and vice versa in development environment
     *
     * @return void
     */
    private function forceRootDomain()
    {
        if (app()->environment() === 'local')
        {
            return;
        }

        URL::forceScheme('https');
        URL::forceRootUrl('https://progtester.eu');
    }

    /**
     * Register blade directives
     * 
     * @return void
     */
    private function registerBladeDirectives()
    {
        Blade::if('moderator', function () { return Auth::check() && Auth::user()->moderator; });

        Blade::directive('breadcrumbs', function ($expression) {
            try 
            {
               return Breadcrumbs::build(eval("return $expression;"));
            }
            catch (Exception $exception) {}
        });
    }

    /**
     * Register CVUT Oauth Socialite driver
     *
     * @return void
     */
    private function registerCvutSocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'cvut',
            function ($app) use ($socialite) {
                $config = $app['config']['services.cvut'];
                return $socialite->buildProvider(CvutAuthenticationProvider::class, $config);
            }
        );
    }
}
