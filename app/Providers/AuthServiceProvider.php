<?php

namespace App\Providers;

use App\Testcase;
use App\AssertTestcase;
use App\Policies\TestcasePolicy;
use App\Policies\AssertTestcasePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Testcase::class => TestcasePolicy::class,
        AssertTestcase::class => AssertTestcasePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
