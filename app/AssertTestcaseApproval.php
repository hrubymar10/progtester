<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssertTestcaseApproval extends Model
{
    protected $fillable = [
        'user_id',
        'assert_testcase_id',
    ];
}
