<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FullOutput extends Model
{
    protected $fillable = [
        'output-data',
        'user_id',
        'testcase_id',
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function testcase()
    {
        return $this->belongsTo(Testcase::class);
    }
}
