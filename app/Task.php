<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Task extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        'subject_id'
    ];

    /**
     * Subject 1..N Task
     * @return BelongsTo
     */
    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }

    /**
     * Task 1..N Testcase
     * @return HasMany
     */
    public function testcases(): HasMany
    {
        return $this->hasMany(Testcase::class);
    }

    /**
     * Task 1..N AssertTestcase
     * @return HasMany
     */
    public function assertTestcases(): HasMany
    {
        return $this->hasMany(AssertTestcase::class);
    }

    /**
     * @return string
     */
    public function archiveName(bool $timestamp = true): string
    {
        $name = $this->name;
        return Str::slug($name . ($timestamp ? (' ' . $this->updated_at) : ''));
    }
}
