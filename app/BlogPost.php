<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $fillable = [
        'title',
        'content',
        'published'
    ];

    public function scopePublished(Builder $query)
    {
        return $query->where('published', true)->get();
    }
}
