<?php

namespace App\Http\Controllers\Api;

use App\Task;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function show(Task $task)
    {
        $columns = [
            'id',
            'task_id',
            'approvals_count',
            'flags_count',
            'bonus',
            'archive'
        ];

        return Task::with('testcases:' . implode(',', $columns))->find($task->id);
    }
}
