<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Testcase;
use Illuminate\Http\Request;

class TestcaseController extends Controller
{
    public function show(Testcase $testcase)
    {
        $columns = [
            'id',
            'username',
            'moderator'
        ];

        return $testcase->with('author:' . implode(',', $columns))->find($testcase->id);
    }
}
