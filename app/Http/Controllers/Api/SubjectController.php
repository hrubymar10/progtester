<?php

namespace App\Http\Controllers\Api;

use App\Subject;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    public function index()
    {
        return Subject::all();
    }

    public function show(Subject $subject)
    {
        return $subject->with('tasks')
            ->select('id', 'name')
            ->get();
    }
}
