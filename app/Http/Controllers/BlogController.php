<?php

namespace App\Http\Controllers;

use App\BlogPost;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Parsedown;

class BlogController extends Controller
{
    public function index()
    {
        $posts = BlogPost::published();
        return view('blog.index', ['posts' => $posts]);
    }

    public function show(BlogPost $post)
    {
        if (!$post->published && !Auth::user()->moderator)
        {
            throw new ModelNotFoundException();
        }

        return view('blog.post', [
            'title' => $post->title,
            'post' => $post,
            'content' => $this->renderMarkDown($post->content)
        ]);
    }

    private function renderMarkDown($markDown)
    {
        $parser = new Parsedown();
        $parser->setUrlsLinked(false);

        $html =  $parser->text($markDown);

        return $html;
    }
}
