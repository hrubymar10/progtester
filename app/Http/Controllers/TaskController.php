<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tasks\CreateTaskRequest;
use App\Service\Archiver\ArchiveConfiguration;
use App\Service\Archiver\Archiver;
use App\Subject;
use App\Task;
use App\Testcase;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TaskController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subject $subject)
    {
        return view('tasks.create', ['subject' => $subject]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Subject $subject, CreateTaskRequest $request)
    {
        $task = new Task(['name' => $request->input('name')]);
        $subject->tasks()->save($task);

        return redirect()->route('tasks.show', $task->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        if ($task->archived)
        {
            return back()->withErrors("Tato úloha byla archivována moderátorem.");
        }

        return view('tasks.show', ['task' => $task]);
    }

    public function download(Task $task, Request $request)
    {
        $invalid = $request->has('include-invalid');
        $bonuses = $request->has('include-bonus');
        $useCrlf = $request->input('line-endings') === 'crlf';

        $query = $task->testcases()->select('id', 'input_data', 'output_data');

        if (!$invalid)
        {
            $query->where('archive', true);
        }

        if (!$bonuses)
        {
            $query->where('bonus', false);
        }

        $testcases = $query->get()->all();

        $configuration = new ArchiveConfiguration($bonuses, $invalid, $useCrlf);
        return Archiver::pack($configuration, $task->archiveName(), $testcases);
    }

    public function archive(Task $task)
    {
        $task->archived = true;
        $task->save();

        $testcases = $task->testcases;
        $asserts = $task->assertTestcases;

        foreach ($testcases as $testcase)
        {
            $testcase->flags()->delete();
            $testcase->approvals()->delete();
        }

        foreach ($asserts as $testcase)
        {
            $testcase->flags()->delete();
            $testcase->approvals()->delete();
        }

        $task->testcases()->delete();
        $task->assertTestcases()->delete();

        return redirect()->route("subjects.show", $task->subject->id);
    }

    public function combine(Task $task)
    {
        $asserts = $task->assertTestcases;
        $rendered = view('stubs.asserts-output', ['asserts' => $asserts])->render();

        return "<pre>$rendered</pre>";
    }
}

