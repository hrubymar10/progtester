<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Messages\CreateMessageRequest;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::with('author')->actual()->get();
        $expired  = Message::with('author')->expired()->get();

        $data = [
            'messages' => $messages,
            'expired'  => $expired,
        ];

        return view('moderator.messages.index', $data);
    }

    public function create()
    {
        return view('moderator.messages.create');
    }

    public function store(CreateMessageRequest $request)
    {
        $message = new Message([
            'content'    => $request->input('content'),
            'expiration' => $request->input('expiration'),
        ]);

        $message->user_id = Auth::id();
        $message->save();

        return redirect()->route('index');
    }

    public function edit(Message $message)
    {
        return view('moderator.messages.edit', ['message' => $message]);
    }

    public function update(Message $message, CreateMessageRequest $request)
    {
        $message->content    = $request->input('content');
        $message->expiration = $request->input('expiration');
        $message->user_id    = Auth::id();

        $message->save();

        return redirect()->route('messages.index');
    }

    public function delete(Message $message)
    {
        $message->delete();

        return redirect()->back();
    }
}
