<?php

namespace App\Http\Controllers;

use App\FullOutput;
use App\Http\Requests\FullOutputUploadRequest;
use App\Testcase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FullOutputController extends Controller
{
    public function create(Testcase $testcase)
    {
        if (!$testcase->editable)
        {
            return back()->withErrors("Tento test nebyl označen.");
        }

        return view('testcases.fix', [ 'testcase' => $testcase ]);
    }

    public function store(Testcase $testcase, FullOutputUploadRequest $request)
    {
        if (!$testcase->editable)
        {
            return back()->withErrors("Tento test nebyl označen.");
        }

        $output = $testcase->fixes()->insert([
            'user_id' => Auth::id(),
            'testcase_id' => $testcase->id,
            'output_data' => $request->input('output-data'),
        ]);

        return redirect()->route('testcases.show', $testcase->id);
    }
}
