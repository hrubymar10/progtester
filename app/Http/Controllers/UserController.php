<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('id')->get();
        return view('moderator.users.index', [ 'users' => $users ]);
    }

    public function elevate(User $user)
    {
        $user->moderator = true;
        $user->save();

        return back();
    }

    public function demote(User $user)
    {
        if ($user->moderator_lock)
        {
            return back()->withErrors("Tomuto uživateli není možné odebrat moderátorská práva.");
        }

        if ($user->id === Auth::id())
        {
            return back()->withErrors("Nelze odebrat moderátorská práva sám sobě.");
        }

        $user->moderator = false;
        $user->save();
        
        return back();
    }
}
