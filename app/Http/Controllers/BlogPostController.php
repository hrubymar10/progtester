<?php

namespace App\Http\Controllers;

use App\BlogPost;
use App\Http\Requests\CreateBlogPostRequest;
use Illuminate\Http\Request;

class BlogPostController extends Controller
{
    public function index()
    {
        return view('moderator.blogposts.index', ['posts' => BlogPost::all()]);
    }

    public function create()
    {
        return view('moderator.blogposts.create');
    }

    public function store(CreateBlogPostRequest $request)
    {
        $post = new BlogPost([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'published' => $request->has('publish'),
        ]);

        $post->save();

        return redirect()->route('blogposts.index');
    }

    public function edit(BlogPost $post)
    {
        return view('moderator.blogposts.edit', ['post' => $post]);
    }

    public function update(CreateBlogPostRequest $request, BlogPost $post)
    {
        $post->update([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'published' => $request->has('publish')
        ]);

        return redirect()->route('blogposts.index');
    }

    public function delete(BlogPost $post)
    {
        $post->delete();
        return redirect()->route('blogposts.index');
    }

    public function publish(BlogPost $post)
    {
        $post->update(['published' => true]);
        return back();
    }

    public function unpublish(BlogPost $post)
    {
        $post->update(['published' => false]);
        return back();
    }
}
