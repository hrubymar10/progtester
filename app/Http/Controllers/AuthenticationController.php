<?php

namespace App\Http\Controllers;

use App\User;
use App\Testcase;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Authentication\LoginRequest;
use App\Http\Requests\Authentication\RegistrationRequest;
use App\Message;
use Exception;
use Illuminate\Http\RedirectResponse;
use Laravel\Socialite\Facades\Socialite;

class AuthenticationController extends Controller
{
    public function index()
    {
        $data = [
            'testcases' => Testcase::count(),
            'messages'  => Message::actual()->get(),
        ];
        
        return view('auth.index', $data);
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        $credentials = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        if (!Auth::attempt($credentials))
        {
            return back()->withErrors("Nesprávné přihlašovací údaje.");
        }

        return redirect()->route('index');
    }

    /**
     * @param RegistrationRequest $request
     * @return RedirectResponse
     */
    public function registration(RegistrationRequest $request): RedirectResponse
    {
        $user = new User([
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
        ]);

        $user->save();

        Auth::login($user);

        return redirect()->route('index');
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        Auth::logout();

        return redirect()->route('index');
    }

    /**
     * @return RedirectResponse
     */
    public function redirectToProvider()
    {
        return Socialite::with('cvut')->redirect();
    }

    /**
     * @return RedirectResponse
     */
    public function handleProviderCallback()
    {
        try 
        {
            $user = Socialite::with('cvut')->user();

            $account = User::where('username',$user->username)->first();

            // User doesn't exist in the database
            if ($account === null)
            {
                // Create a new user
                $account = new User([
                    'username' => $user->username,
                    'token' => $user->token,
                ]);
            }
            else
            {
                $account->token = $user->token;
            }

            $account->save();

            Auth::login($account);

            return redirect()->route('index');
        }
        catch (Exception $exception)
        {
            $route = 'authentication';
            $error = app()->environment() === 'local' ? $exception->getMessage() : "Nepovedlo se spojit s auth serverem ČVUT";

            if (Auth::check())
            {
                $route = 'index';
            }

            return redirect()->route($route)->withErrors($error);
        }
    }
}
