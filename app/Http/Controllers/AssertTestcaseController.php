<?php

namespace App\Http\Controllers;

use App\Task;
use App\AssertTestcase;
use App\AssertTestcaseFlag;
use Illuminate\Http\Request;
use App\AssertTestcaseApproval;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\AssertTestcaseUploadRequest;

class AssertTestcaseController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Task $task)
    {
        return view('asserts.create', ['task' => $task]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Task $task, AssertTestcaseUploadRequest $request)
    {
        $testcase = new AssertTestcase([
            'content' => $request->input('content'),
            'user_id' => Auth::id(),
            'bonus' => $request->has('bonus'),
        ]);

        $task->assertTestcases()->save($testcase);

        return redirect()->route('asserts.show', $testcase->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testcase  $testcase
     * @return \Illuminate\Http\Response
     */
    public function show(AssertTestcase $assertTestcase)
    {
        $sum = $assertTestcase->approvals_count + $assertTestcase->flags_count; 

        return view('asserts.show', ['testcase' => $assertTestcase, 'sum' => $sum ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testcase  $testcase
     * @return \Illuminate\Http\Response
     */
    public function edit(AssertTestcase $assertTestcase)
    {
        $this->authorize('edit', $assertTestcase);

        return view('asserts.edit', ['testcase' => $assertTestcase, 'task' => $assertTestcase->task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testcase  $testcase
     * @return \Illuminate\Http\Response
     */
    public function update(AssertTestcaseUploadRequest $request, AssertTestcase $assertTestcase): RedirectResponse
    {
        $this->authorize('edit', $assertTestcase);

        $assertTestcase->content = $request->input('content') . "\n";
        $assertTestcase->bonus = $request->has('bonus');
        $assertTestcase->save();

        return redirect()->route('asserts.show', $assertTestcase->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testcase  $testcase
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssertTestcase $assertTestcase): RedirectResponse
    {
        $this->authorize('destroy', $assertTestcase);

        $response = redirect()->route('tasks.show', $assertTestcase->task->id);
        $assertTestcase->delete();

        return $response;
    }

    public function approve(AssertTestcase $testcase): RedirectResponse
    {
        $this->authorize('approve', $testcase);

        $approval = new AssertTestcaseApproval([
            'assert_testcase_id' => $testcase->id,
            'user_id' => Auth::id(),
        ]);

        $count = ++$testcase->approvals_count;

        if ($count >= $testcase->flags_count)
        {
            $testcase->archive = true;
        }

        $testcase->save();
        $approval->save();

        return back();
    }

    public function flag(AssertTestcase $testcase): RedirectResponse
    {
        $this->authorize('flag', $testcase);

        $flag = new AssertTestcaseFlag([
            'assert_testcase_id' => $testcase->id,
            'user_id' => Auth::id(),
        ]);

        $count = ++$testcase->flags_count;

        if ($count > $testcase->approvals_count)
        {
            $testcase->archive = false;
        }

        $testcase->save();
        $flag->save();

        return back();
    }
}
