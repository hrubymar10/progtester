<?php

namespace App\Http\Controllers;

use App\Service\Archiver\Archiver;
use App\Task;
use App\Testcase;
use App\TestcaseApproval;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Testcases\TestcaseUploadRequest;
use App\TestcaseFlag;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TestcaseController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Task $task)
    {
        return view('testcases.create', ['task' => $task]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Task $task
     * @param TestcaseUploadRequest $request
     * @return Response
     */
    public function store(Task $task, TestcaseUploadRequest $request)
    {
        $testcase = new Testcase([
            'input_data' => $this->normalizeText($request->input('input-data') ?? ''),
            'output_data' => $this->normalizeText($request->input('output-data')),
            'user_id' => Auth::id(),
            'description' => $request->input('description'),
            'editable' => preg_match('/<skipping [0-9]+ B>/i', $request->input('output-data')),
            'bonus' => $request->has('bonus'),
        ]);

        $task->testcases()->save($testcase);

        $this->deleteArchivesCache($task);

        // Rewrites the updated_at timestamp
        $task->touch();

        return redirect()->route('testcases.show', $testcase->id);
    }

    /**
     * Display the specified resource.
     *
     * @param Testcase $testcase
     * @return Response
     */
    public function show(Testcase $testcase)
    {
        $sum = $testcase->approvals_count + $testcase->flags_count;

        $previous = $testcase->task->testcases()->where('id', '<', $testcase->id)->orderBy('id', 'desc')->first();
        $next = $testcase->task->testcases()->where('id', '>', $testcase->id)->orderBy('id', 'asc')->first();

        $data = [
            'testcase' => $testcase,
            'sum' => $sum,
            'next' => $next,
            'previous' => $previous,
        ];

        return view('testcases.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Testcase $testcase
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Testcase $testcase)
    {
        $this->authorize('edit', $testcase);

        return view('testcases.edit', ['testcase' => $testcase, 'task' => $testcase->task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TestcaseUploadRequest $request
     * @param Testcase $testcase
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(TestcaseUploadRequest $request, Testcase $testcase): RedirectResponse
    {
        $this->authorize('edit', $testcase);

        $testcase->input_data = $request->input('input-data', '') . "\n";
        $testcase->output_data = $request->input('output-data') . "\n";
        $testcase->bonus = $request->has('bonus');
        $testcase->description = $request->input('description');
        $testcase->editable = preg_match('/<skipping [0-9]+ B>/i', $request->input('output-data'));
        $testcase->save();

        $this->deleteArchivesCache($testcase->task);

        $testcase->task->touch();

        return redirect()->route('testcases.show', $testcase->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Testcase $testcase
     * @return RedirectResponse
     * @throws AuthorizationException
     * @throws Exception
     */
    public function destroy(Testcase $testcase): RedirectResponse
    {
        $this->authorize('destroy', $testcase);

        $response = redirect()->route('tasks.show', $testcase->task->id);
        $testcase->delete();

        return $response;
    }

    /**
     * @param Testcase $testcase
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function approve(Testcase $testcase): RedirectResponse
    {
        $this->authorize('approve', $testcase);

        $approval = new TestcaseApproval([
            'testcase_id' => $testcase->id,
            'user_id' => Auth::id(),
        ]);

        $count = ++$testcase->approvals_count;

        if ($count >= $testcase->flags_count)
        {
            $testcase->archive = true;
        }

        $testcase->save();
        $approval->save();

        return back();
    }

    /**
     * @param Testcase $testcase
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function flag(Testcase $testcase): RedirectResponse
    {
        $this->authorize('flag', $testcase);

        $flag = new TestcaseFlag([
            'testcase_id' => $testcase->id,
            'user_id' => Auth::id(),
        ]);

        $count = ++$testcase->flags_count;

        if ($count > $testcase->approvals_count)
        {
            $testcase->archive = false;
        }

        $testcase->save();
        $flag->save();

        return back();
    }

    /**
     * @param Task $task
     */
    private function deleteArchivesCache(Task $task)
    {
        Archiver::deleteAll($task->archiveName(false));
    }

    /**
     * @param string|null $text
     * @return string
     */
    private function normalizeText(string $text): string
    {
        // Replace all CRLF line endings with LF
        $text = str_replace("\r", "", $text);

        // Trim trailing newlines if there are any
        $text = trim($text);

        // And finally append the last, required trailing newline
        return $text . "\n";
    }
}
