<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class RequiresModeratorPrivileges
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Unless user is a moderator deny this request with HTTP 403 response
        if (!Auth::check() || !Auth::user()->moderator)
        {
            app()->abort(Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
