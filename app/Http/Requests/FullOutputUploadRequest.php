<?php

namespace App\Http\Requests;

use App\Testcase;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class FullOutputUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Testcase $testcase
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'output-data' => 'required'
        ];
    }
}
