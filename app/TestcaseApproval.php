<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestcaseApproval extends Model
{
    protected $fillable = [
        'user_id',
        'testcase_id',
    ];
}
