<?php

namespace App\Helpers;


final class Breadcrumbs
{
    /**
     * @param array $source
     * @return string
     */
    public static function build(array $source): string
    {
        // Tohle řešení není moc cool, ale už to používám na hodně místech...
        $built = "<li class='breadcrumb-item'><a href='" . route('index') . "'>Home</a></li>";

        foreach ($source as $name => $link)
        {
            $name = htmlentities($name);

            if ($link === null)
            {
                $built .= "<li class='breadcrumb-item active'>$name</li>";
            }
            else
            {
                $built .= "<li class='breadcrumb-item'><a href='$link'>$name</a></li>";
            }
        }

        return $built;
    }
}
