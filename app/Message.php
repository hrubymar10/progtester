<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'content',
        'expiration',
        'user_id'
    ];

    public function scopeActual(Builder $builder)
    {
        return $builder->where('expiration', '>=', today());
    }

    public function scopeExpired(Builder $builder)
    {
        return $builder->where('expiration', '<', today());
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
