<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssertTestcase extends Model
{
    protected $fillable = [
        'content',
        'user_id',
        'bonus',
    ];

    protected $hidden = [
        'task_id',
        'user_id',
    ];
    
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    public function approvals()
    {
        return $this->hasMany(AssertTestcaseApproval::class);
    }

    public function flags()
    {
        return $this->hasMany(AssertTestcaseFlag::class);
    }
}
