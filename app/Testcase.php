<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Testcase
 * @package App
 * @method
 * @method static count()
 */
class Testcase extends Model
{
    protected $fillable = [
        'input_data',
        'output_data',
        'user_id',
        'bonus',
        'editable',
        'description',
    ];

    protected $hidden = [
        'task_id',
        'user_id',
    ];

    /**
     * Testcase N..1 Task
     * @return BelongsTo
     */
    public function task(): BelongsTo
    {
        return $this->belongsTo(Task::class);
    }

    /**
     * Testcase 1..N TestcaseApproval
     * @return HasMany
     */
    public function approvals(): HasMany
    {
        return $this->hasMany(TestcaseApproval::class);
    }

    /**
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Testcase 1..N TestcaseFlag
     * @return HasMany
     */
    public function flags(): HasMany
    {
        return $this->hasMany(TestcaseFlag::class);
    }

    public function fixes(): HasMany
    {
        return $this->HasMany(FullOutput::class);
    }
}
