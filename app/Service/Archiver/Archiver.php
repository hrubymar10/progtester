<?php

namespace App\Service\Archiver;

use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;

final class Archiver
{
    private static $inputSuffix = "_in.txt";

    private static $outputSuffix = '_out.txt';

    private static $archivesPath = 'packed/';


    /**
     * @param ArchiveConfiguration $configuration
     * @param string $name
     * @param array $tests
     * @return Factory|View
     */
    public static function pack(ArchiveConfiguration $configuration, string $name, array $tests)
    {
        $disk = static::getDisk();

        $archive = new ZipFile();
        $file = static::$archivesPath . $configuration->buildName($name) . '.zip';

        if (!$disk->exists($file))
        {
            try
            {
                foreach ($tests as $test)
                {
                    $name = str_pad($test->id, 5, '0', STR_PAD_LEFT);
                    $compression = ZipFile::METHOD_STORED;

                    $archive->addFromString($name . static::$inputSuffix, static::fixLineEndings($test->input_data, $configuration->usesCrlf), $compression);
                    $archive->addFromString($name . static::$outputSuffix, static::fixLineEndings($test->output_data, $configuration->usesCrlf), $compression);
                }

                touch($disk->path($file));
                $archive->saveAsFile($disk->path($file));
            }
            catch (ZipException $exception)
            {
                return view('error');
            }
            finally
            {
                $archive->close();
            }
        }

        return $disk->download($file);
    }

    public static function deleteAll(string $task)
    {
        $disk = static::getDisk();
        $root = $disk->path('');

        $archives = File::glob($root . static::$archivesPath . $task . '*.zip');

        foreach ($archives as $archive)
        {
            $disk->delete(str_replace($root, '', $archive));
        }
    }

    /**
     * @param string $name
     * @return void
     */
    public static function delete(string $name)
    {
        $disk = static::getDisk();
        $archive = static::$archivesPath . $name . '.zip';

        if ($disk->exists($archive))
        {
            $disk->delete($archive);
        }
    }

    private static function getDisk()
    {
        return Storage::disk('local');
    }

    private static function fixLineEndings(string $data, bool $crlf)
    {
        if ($crlf)
        {
            return preg_replace('/[^\r]\n/', "\r\n", $data);
        }

        return preg_replace('/\r\n/', "\n", $data);
    }
}
