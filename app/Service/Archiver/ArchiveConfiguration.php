<?php


namespace App\Service\Archiver;


use Illuminate\Support\Str;

final class ArchiveConfiguration
{
    /**
     * @var bool
     */
    public $includesBonus;

    /**
     * @var bool
     */
    public $includesFlagged;

    /**
     * @var bool
     */
    public $usesCrlf;

    public function __construct($includesBonus = false, $includesFlagged = false, $usesCrlf = false)
    {
        $this->includesBonus = $includesBonus;
        $this->includesFlagged = $includesFlagged;
        $this->usesCrlf = $usesCrlf;
    }

    public function buildName(string $base)
    {
        $name = $base;

        if ($this->includesBonus)
        {
            $name .= '-bonus';
        }

        if ($this->includesFlagged)
        {
            $name .= '-flagged';
        }

        $name .= ($this->usesCrlf ? '-crlf' : '-lf');

        return Str::slug($name);
    }
}
