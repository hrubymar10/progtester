<?php

namespace App\Service;

use Illuminate\Support\Arr;
use Laravel\Socialite\Two\User;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;

class CvutAuthenticationProvider extends AbstractProvider implements ProviderInterface
{
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://auth.fit.cvut.cz/oauth/authorize', $state);
    }

    protected function getTokenUrl()
    {
        return 'https://auth.fit.cvut.cz/oauth/token'; 
    }

    public function getAccessTokenResponse($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), 
        [
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret),
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => $this->getTokenFields($code),
        ]);

        $content = json_decode($response->getBody(), true);

        return $content;
    }

    protected function getTokenFields($code)
    {
        return Arr::add(parent::getTokenFields($code), 'grant_type', 'authorization_code');
    }

    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get('https://auth.fit.cvut.cz/oauth/check_token', [ 'query' => [ 'token' => $token ]]);

        return json_decode($response->getBody(), true);
    }

    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'username' => $user['user_name'],
        ]);
    }
}