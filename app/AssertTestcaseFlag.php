<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssertTestcaseFlag extends Model
{
    protected $fillable = [
        'user_id',
        'assert_testcase_id',
    ];
}
