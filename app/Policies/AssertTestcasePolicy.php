<?php

namespace App\Policies;

use App\AssertTestcase;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssertTestcasePolicy
{
    use HandlesAuthorization;

    /**
     * @param $user
     * @param $ability
     * @return bool
     */
    public function before($user, $ability)
    {
        $excludedAbilities = [ 'approve', 'flag' ];

        if ($user->moderator && !in_array($ability, $excludedAbilities))
        {
            return true;
        }
    }

    /**
     * @param User $user
     * @param Testcase $testcase
     * @return bool
     */
    public function edit(User $user, AssertTestcase $testcase): bool
    {
        return $this->isAuthor($user, $testcase);
    }

    /**
     * @param User $user
     * @param Testcase $testcase
     * @return bool
     */
    public function destroy(User $user, AssertTestcase $testcase): bool
    {
        return $this->isAuthor($user, $testcase);
    }

    /**
     * @param User $user
     * @param Testcase $testcase
     * @return bool
     */
    public function approve(User $user, AssertTestcase $testcase): bool
    {
        return !$this->isAuthor($user, $testcase) && !$testcase->approvals->pluck('user_id')->contains($user->id);
    }

    /**
     * @param User $user
     * @param Testcase $testcase
     * @return bool
     */
    public function flag(User $user, AssertTestcase $testcase): bool
    {
        return !$this->isAuthor($user, $testcase) && !$testcase->flags->pluck('user_id')->contains($user->id);
    }

    /**
     * @param User $user
     * @param Testcase $testcase
     * @return bool
     */
    private function isAuthor(User $user, AssertTestcase $testcase): bool
    {
        return $testcase->author->id === $user->id;
    }
}
