<?php

use App\Http\Controllers\Api\SubjectController;
use App\Http\Controllers\Api\TaskController;
use App\Http\Controllers\Api\TestcaseController;
use App\Http\Controllers\DocumentationController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {

    Route::get('/', [DocumentationController::class, 'api']);

    Route::get('/subjects', [SubjectController::class, 'index']);
    Route::get('/subjects/{subject}', [SubjectController::class, 'show']);

    Route::get('/task/{task}', [TaskController::class, 'show']);

    Route::get('/testcase/{testcase}', [TestcaseController::class, 'show']);

});
