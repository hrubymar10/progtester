<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\FullOutputController;
use App\Http\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TestcaseController;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Controllers\AssertTestcaseController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\BlogPostController;
use App\Http\Middleware\RequiresModeratorPrivileges;

Route::middleware(RedirectIfAuthenticated::class)->group(function () {
    Route::get('/authentication', [AuthenticationController::class, 'index'])->name('authentication');

    Route::get('/authentication/redirect', [AuthenticationController::class, 'redirectToProvider'])->name('authentication.redirect');
    Route::get('/authentication/callback', [AuthenticationController::class, 'handleProviderCallback'])->name('authentication.callback');

    Route::post('/login', [AuthenticationController::class, 'login'])->name('login');
    Route::post('/registration', [AuthenticationController::class, 'registration'])->name('registration');
});

Route::get('/logout', [AuthenticationController::class, 'logout'])->name('logout');

Route::middleware(RequiresModeratorPrivileges::class)->group(function () {
    Route::get('/subject/add', [SubjectController::class, 'create'])->name('subjects.create');
    Route::post('/subject/add', [SubjectController::class, 'store'])->name('subjects.store');
    Route::delete('/subject/{subject}', [SubjectController::class, 'destroy'])->name('subjects.destroy');

    Route::get('/subject/{subject}/add-task', [TaskController::class, 'create'])->name('tasks.create');
    Route::post('/subject/{subject}/add-task', [TaskController::class, 'store'])->name('tasks.store');

    Route::delete('/task/{task}', [TaskController::class, 'destroy'])->name('tasks.destroy');
    Route::any('/task/{task}/archive', [TaskController::class, 'archive'])->name('tasks.archive');

    Route::get('/users', [UserController::class, 'index'])->name('users.index');
    Route::get('/users/{user}/elevate', [UserController::class, 'elevate'])->name('users.elevate');
    Route::get('/users/{user}/demote', [UserController::class, 'demote'])->name('users.demote');

    Route::get('/messages', [MessageController::class, 'index'])->name('messages.index');
    Route::get('/messages/create',[MessageController::class, 'create'])->name('messages.create');
    Route::post('/messages/create',[MessageController::class, 'store'])->name('messages.store');
    Route::get('/messages/{message}/edit',[MessageController::class, 'edit'])->name('messages.edit');
    Route::post('/messages/{message}/update',[MessageController::class, 'update'])->name('messages.update');
    Route::any('/messages/{message}/delete',[MessageController::class, 'delete'])->name('messages.delete');

    Route::get('/blogposts', [BlogPostController::class, 'index'])->name('blogposts.index');
    Route::get('/blogposts/create', [BlogPostController::class, 'create'])->name('blogposts.create');
    Route::post('/blogposts/create', [BlogPostController::class, 'store'])->name('blogposts.store');
    Route::get('/blogposts/{post}/edit', [BlogPostController::class, 'edit'])->name('blogposts.edit');
    Route::post('/blogposts/{post}/update', [BlogPostController::class, 'update'])->name('blogposts.update');
    Route::post('/blogposts/{post}/delete', [BlogPostController::class, 'delete'])->name('blogposts.delete');
    Route::any('/blogposts/{post}/publish', [BlogPostController::class, 'publish'])->name('blogposts.publish');
    Route::any('/blogposts/{post}/unpublish', [BlogPostController::class, 'unpublish'])->name('blogposts.unpublish');
});

Route::middleware(Authenticate::class)->group(function () {
    Route::get('/', [SubjectController::class, 'index'])->name('index');

    Route::get('/subject/{subject}', [SubjectController::class, 'show'])->name('subjects.show');

    Route::get('/task/{task}', [TaskController::class, 'show'])->name('tasks.show');
    Route::get('/task/{task}/add-testcase', [TestcaseController::class, 'create'])->name('testcases.create');
    Route::post('/task/{task}/add-testcase', [TestcaseController::class, 'store'])->name('testcases.store');

    Route::post('/task/{task}/download-testcases', [TaskController::class, 'download'])->name('tasks.download');

    Route::get('/testcase/{testcase}', [TestcaseController::class, 'show'])->name('testcases.show');

    Route::get('/testcase/{testcase}/edit', [TestcaseController::class, 'edit'])->name('testcases.edit');
    Route::post('/testcase/{testcase}/update', [TestcaseController::class, 'update'])->name('testcases.update');
    Route::delete('/testcase/{testcase}', [TestcaseController::class, 'destroy'])->name('testcases.destroy');

    Route::get('/testcase/{testcase}/fix', [FullOutputController::class, 'create'])->name('full-output.create');
    Route::post('/testcase/{testcase}/fix', [FullOutputController::class, 'store'])->name('full-output.store');

    Route::get('/testcase/{testcase}/approve', [TestcaseController::class, 'approve'])->name('testcases.approve');
    Route::get('/testcase/{testcase}/flag', [TestcaseController::class, 'flag'])->name('testcases.flag');

    Route::get('/task/{task}/add-assert', [AssertTestcaseController::class, 'create'])->name('asserts.create');
    Route::post('/task/{task}/add-assert', [AssertTestcaseController::class, 'store'])->name('asserts.store');

    Route::get('/assert/{task}/combine', [TaskController::class, 'combine'])->name('tasks.combine');
    Route::get('/assert/{assertTestcase}', [AssertTestcaseController::class, 'show'])->name('asserts.show');

    Route::get('/assert/{assertTestcase}/edit', [AssertTestcaseController::class, 'edit'])->name('asserts.edit');
    Route::post('/assert/{assertTestcase}/update', [AssertTestcaseController::class, 'update'])->name('asserts.update');
    Route::delete('/assert/{assertTestcase}', [AssertTestcaseController::class, 'destroy'])->name('asserts.destroy');

    Route::get('/assert/{testcase}/approve', [AssertTestcaseController::class, 'approve'])->name('asserts.approve');
    Route::get('/assert/{testcase}/flag', [AssertTestcaseController::class, 'flag'])->name('asserts.flag');

    Route::get('/blog', [BlogController::class, 'index'])->name('blog.index');
    Route::get('/blog/post/{post}', [BlogController::class, 'show'])->name('blog.show');
});
