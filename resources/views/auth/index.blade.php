@extends('layout.master')

@section('nav')
    {!! breadcrumbs([]) !!}
@endsection

@section('content')
    <h1>Progtester</h1>
    <p class="mb-3 text-muted">Komunitní databáze testů k úlohám na ProgTestu</p>

    @include('includes.messages')

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="px-3 rounded bg-dark text-light pt-3 pb-1">
                <h3>Přihlášení</h3>
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="login-username">Uživatelské jméno</label>
                        <input class="form-control" id="login-username" type="text" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="login-password">Heslo</label>
                        <input class="form-control" id="login-password" type="password" name="password" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary">Přihlásit se</button>
                        <a href="{{ route('authentication.redirect') }}" class="ml-2 btn btn-success">Přihlásit se účtem ČVUT</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="p-3">
                <h3 class="uk-card-title">Registrace</h3>
                <form action="{{ route('registration') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="registration-username">Uživatelské jméno</label>
                        <input class="form-control" id="registration-username" type="text" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="registration-password">Heslo</label>
                        <input class="form-control" id="registration-password" type="password" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="registration-password-confirmation">Heslo znovu</label>
                        <input class="form-control" id="registration-password-confirmation" type="password" name="password_confirmation" required>
                    </div>
                    <button class="btn btn-secondary">Registrovat</button>
                </form>
            </div>
        </div>
    </div>
@endsection
