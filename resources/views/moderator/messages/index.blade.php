@extends('layout.master')

@section('nav')
    {!! breadcrumbs([
        'Správa oznámení' => null,
    ]) !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12 mt-3">
            <h1>Aktuální zprávy</h1>
            <div class="mb-3">
                <a class="btn btn-primary" href="{{ route('messages.create') }}">Přidat novou zprávu</a>
            </div>
            <div class="list-group">
                @foreach ($messages as $message)
                    <div class="list-group-item d-flex justify-content-between align-items-center">
                        <div>
                            <b>{{ $message->content }}</b>
                            <small>&mdash; Přidal <b>{{ $message->author->username }}</b> &mdash; Expiruje {{ $message->expiration }}</small>
                        </div>
                        <div>
                            <a href="{{ route('messages.edit', $message->id) }}" class="btn btn-secondary">Upravit</a>
                            <a href="{{ route('messages.delete', $message->id) }}" class="btn btn-danger" onclick="return confirm('Opravdu smazat?')">Smazat</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-sm-12 mt-3">
            <h1>Po expiraci</h1>
            @foreach ($expired as $message)
                <div class="list-group-item d-flex justify-content-between align-items-center">
                    <div>
                        <b>{{ $message->content }}</b>
                        <small>&mdash; Přidal <b>{{ $message->author->username }}</b> &mdash; Expiruje {{ $message->expiration }}</small>
                    </div>
                    <div>
                        <a href="{{ route('messages.edit', $message->id) }}" class="btn btn-secondary">Upravit</a>
                        <a href="{{ route('messages.delete', $message->id) }}" class="btn btn-danger" onclick="return confirm('Opravdu smazat?')">Smazat</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
