@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Správa oznámení' => route('messages.index'),
            'Přidat novou zprávu' => null
        ]);
    !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Přidat novou zprávu</h1>
            <div class="uk-card uk-card-body uk-card-default">
                <form action="{{ route('messages.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="content">Obsah oznámení (bez HTML)</label>
                        <input class="form-control" id="content" type="text" name="content" required>
                    </div>
                    <div class="form-group">
                        <label for="expiration">Datum expirace zprávy</label>
                        <input class="form-control" id="expiration" type="date" name="expiration" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Přidat</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection