@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Správa oznámení' => route('messages.index'),
            'Upravit zprávu #' . $message->id => null
        ]);
    !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Upravit zprávu #{{ $message->id }}</h1>
            <div class="uk-card uk-card-body uk-card-default">
                <form action="{{ route('messages.update', $message->id) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="content">Obsah oznámení (bez HTML)</label>
                        <input class="form-control" id="content" type="text" name="content" required value="{{ $message->content }}">
                    </div>
                    <div class="form-group">
                        <label for="expiration">Datum expirace zprávy</label>
                        <input class="form-control" id="expiration" type="date" name="expiration" required value={{ $message->expiration }}>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Přidat</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection