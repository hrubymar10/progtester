@extends('layout.master')

@section('nav')
    {!! breadcrumbs([
        'Správa uživatelů' => null
    ]) !!}
@endsection

@section('content')
    <h1>Správa uživatelů</h1>
    <small>Celkem zaregistrováno <b>{{ $users->count() }}</b> uživatelů</small>

    <div class="row">
        <div class="col-sm-12">
            <ul class="list-group">
                @foreach ($users as $user)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div class="">
                            #{{ $user->id }}
                            <b>{{ $user->username }}</b>
                            @if ($user->moderator)
                                <span class="badge badge-{{ $user->moderator_lock ? 'danger' : 'primary' }}">MOD</span>                        
                            @endif

                            @if ($user->password !== null)
                                <span class="badge badge-secondary">BASIC</span>                        
                            @endif

                            @if ($user->token !== null)
                                <span class="badge badge-success">OAUTH</span>                        
                            @endif
                        </div>
                        <div>
                            <div class="d-inline mr-2">
                                @if ($user->moderator)
                                    @unless ($user->moderator_lock)
                                        <a href="{{ route('users.demote', $user->id) }}" class="btn btn-secondary">Odebrat moderátorská práva</a>
                                    @endunless
                                @else
                                    <a href="{{ route('users.elevate', $user->id) }}" class="btn btn-secondary">Udělit moderátorská práva</a>
                                @endif
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
   </div>
@endsection