@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Správa příspěvků' =>  route('blogposts.index'),
            'Upravit příspěvek #' . $post->id => null
        ])
     !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Upravit příspěvek #{{ $post->id }}</h1>
            <hr>
            <form action="{{ route('blogposts.update', $post->id) }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Nadpis</label>
                    <input type="text" name="title" id="title" class="form-control" required value="{{ $post->title }}">
                </div>
                <div class="form-group">
                    <label for="content">Obsah (MarkDown)</label>
                    <textarea name="content" id="content" class="form-control" required rows="20">{{ $post->content }}</textarea>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="publish" name="publish" {{ $post->published ? 'checked' : '' }}>
                        <label class="custom-control-label" for="publish">Publikovat</label>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Přidat</button>
                </div>
            </form>
        </div>
    </div>
@endsection
