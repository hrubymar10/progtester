@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Správa příspěvků' => null
        ])
     !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Správa příspěvků</h1>
            <hr>
            <div class="my-3">
                <a href="{{ route('blogposts.create') }}" class="btn btn-success">Přidat příspěvek</a>
            </div>
        </div>
        <div class="col-sm-12">
            @forelse($posts as $post)
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title">{{ $post->title }}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{ $post->published ? 'Publikovaný' : 'Skrytý' }}</h6>
                        <a href="{{ route('blog.show', $post->id) }}" class="card-link" target="_blank">Zobrazit</a>
                        <a href="{{ route('blogposts.edit', $post->id) }}" class="card-link">Upravit</a>
                        @if ($post->published)
                            <a href="{{ route('blogposts.unpublish', $post->id) }}" class="card-link">Zrušit publikaci</a>
                        @else
                            <a href="{{ route('blogposts.publish', $post->id) }}" class="card-link">Publikovat</a>
                        @endif
                        <form action="{{ route('blogposts.delete', $post->id) }}" method="post" class="pl-3 d-inline-block">
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Opravdu smazat?');">Smazat</button>
                        </form>
                    </div>
                </div>
            @empty
                <p>Nic tu není, Járo, napiš něco...</p>
            @endforelse
        </div>
    </div>
@endsection
