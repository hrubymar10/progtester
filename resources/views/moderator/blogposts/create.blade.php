@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Správa příspěvků' =>  route('blogposts.index'),
            'Přidat nový příspěvek' => null
        ])
     !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Přidat nový příspěvek</h1>
            <hr>
            <form action="{{ route('blogposts.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Nadpis</label>
                    <input type="text" name="title" id="title" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="content">Obsah (MarkDown)</label>
                    <textarea name="content" id="content" class="form-control" required rows="20"></textarea>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="publish" name="publish">
                        <label class="custom-control-label" for="publish">Rovnou publikovat</label>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Přidat</button>
                </div>
            </form>
        </div>
    </div>
@endsection
