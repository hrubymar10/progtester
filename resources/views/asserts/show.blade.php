@extends('layout.master')

@push('links')
    <link rel="stylesheet"
        href="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.16.2/build/styles/monokai-sublime.min.css">
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.16.2/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
@endpush

@section('nav')
    {!!
        breadcrumbs([
            'Předměty' => route('index'),
            $testcase->task->subject->name => route('subjects.show', $testcase->task->subject->id),
            $testcase->task->name => route('tasks.show', $testcase->task->id),
            'Assert #' . $testcase->id => null
        ])    
    !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Assert #{{ $testcase->id }}</h1>
            <div class="text-{{ $testcase->archive ? 'muted' : 'danger' }}">
                {{ $testcase->approvals_count }} &times; Potvrzeno,
                {{ $testcase->flags_count }} &times; Nahlášeno jako nefunkční
            </div>
            <div class="d-flex justift-content-start align-items-center py-3">
                <small>
                    {{ $testcase->task->name }} &mdash; {{ $testcase->created_at }}
                </small>
                @if ($testcase->bonus)
                    <div class="badge badge-warning ml-2">
                        Assert k bonusové otázce
                    </div>
                @endif
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="assert">Kód</label>
                <pre><code class="cpp" id="assert">{{ $testcase->content }}</code></pre>
            </div>
            <div class="form-group d-flex justify-content-between align-items-center">
                <button class="btn btn-light" onclick="javascript:copy('assert', this)">Kopírovat</button>
                <div>
                    @can('edit', $testcase)
                        <a class="btn btn-secondary" href="{{ route('asserts.edit', $testcase->id) }}">Upravit</a>
                    @endcan
                    @can('destroy', $testcase)
                        <form action="{{ route('asserts.destroy', $testcase->id) }}" style="display: inline-block;" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Opravdu smazat test?');">Smazat</button>
                        </form>
                    @endcan
                </div>
            </div>
        </div>
            <script>
                function copy (id, button)
                {
                    let source = document.getElementById(id);

                    if (typeof source !== 'undefined')
                    {
                            const el = document.createElement('textarea');
                                el.value = source.innerText;

                            document.body.appendChild(el);
                            el.select();

                            document.execCommand('copy');
                            document.body.removeChild(el);

                        button.innerText = "Zkopírováno";
                    }
                }
            </script>
            <div id="hidden-vagner"><img src="" id="hidden-vagner-img"></div>
            <style>
                #hidden-vagner {
                    position: fixed;
                    left: 0px;
                    top: calc(80% - 100px);
                    perspective: 100px;
                    transform-origin: 50% 100%;
                    transform: translateX(-200px) rotateY(90deg) rotate(90deg);
                    transition: 0.3s all;
                    height: 200px;
                }
                #hidden-vagner.shown {
                    transform: translateX(-100px) rotateY(0deg) rotate(90deg);
                }
                #hidden-vagner-img {
                    height: 200px;
                }
            </style>
            <script>
                const yes = "{{ asset('img/yes.png') }}"
                const nope = "{{ asset('img/nope.png') }}"
                const vagner = document.getElementById("hidden-vagner");
                const img = document.getElementById("hidden-vagner-img");
            </script>
            <div class="col-sm-12 d-flex justify-content-between align-items-center">
                <div>
                    @can('approve', $testcase)
                        <a href="{{ route('asserts.approve', $testcase->id) }}" class="btn btn-success mb-3" id="approve">Potvrdit správnost assertu</a>
                        <script>
                            document.getElementById('approve').addEventListener('mouseover', function () { vagner.classList.add('shown'); img.setAttribute('src', yes); });
                            document.getElementById('approve').addEventListener('mouseout', function () { vagner.classList.remove('shown'); }); 
                        </script>
                    @endcan
                    @can('flag', $testcase)
                        <a href="{{ route('asserts.flag', $testcase->id) }}" class="btn btn-danger mb-3" id="flag">Nahlásit jako nefunkční</a>
                        <script>
                            document.getElementById('flag').addEventListener('mouseover', function () { vagner.classList.add('shown'); img.setAttribute('src', nope); });
                            document.getElementById('flag').addEventListener('mouseout', function () { vagner.classList.remove('shown'); });
                        </script>
                    @endcan
                </div>
            </div>
        </div>
    </div>   
</div>
@endsection