@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Předměty' => route('index'),
            $task->subject->name => route('subjects.show', $task->subject->id),
            $task->name => route('tasks.show', $task->id),
            'Upravit assert #' . $testcase->id => null
        ])
    !!} 
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Upravit assert #{{ $testcase->id }}</h1>
        </div>

        <div class="col-sm-12">
            <form action="{{ route('asserts.update', $testcase->id) }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="content">Část kódu s asserty</label>
                            <textarea name="content" rows="10" id="content" class="form-control" required style="font-family: monospace;">{{ $testcase->content }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="bonus" name="bonus" {{ $testcase->bonus ? 'checked' : ''}}>
                        <label class="custom-control-label" for="bonus">Bonusová otázka?</label>
                    </div>
                </div>
                <button class="btn btn-primary" onclick="this.disabled = true; this.form.submit()">Uložit</button>
            </form>
        </div>   
    </div>
@endsection