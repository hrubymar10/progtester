@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Blog' => route('blog.index'),
            $title => null
        ])
     !!}
@endsection

@section('content')
    <article class="my-5">
        @moderator
            <a href="{{ route('blogposts.edit', $post->id) }}" class="btn btn-primary">Upravit</a>
            @unless($post->published)
                <div class="alert alert-danger">
                    Tento příspěvek není publikovaný
                    <hr>
                    <a href="{{ route('blogposts.publish', $post->id) }}" class="alert-link">Publikovat</a>
                </div>
            @endunless
        @endmoderator
        <div class="px-5 mt-3">
            <h1>{{ $title }}</h1>
            <hr>
            {!! $content !!}
        </div>
    </article>
@endsection
