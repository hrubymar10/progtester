@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Blog' => null
        ])
     !!}
@endsection

@section('content')
    <h1 class="display-4">Progtester Blog</h1>
    <hr>
    @moderator
    <div class="my-3">
        <a href="{{ route('blogposts.create') }}" class="btn btn-success">Napsat příspěvek</a>
    </div>
    @endmoderator
    <div>
        @forelse ($posts->reverse() as $post)
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">{{ $post->title }}</h5>
                    <a href="{{ route('blog.show', $post->id) }}" class="card-link">Zobrazit</a>
                </div>
            </div>
        @empty
            Zatím zde nejsou žádné příspěvky. Jára něco napíše...
        @endforelse
    </div>

@endsection
