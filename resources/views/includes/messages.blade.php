
@if (isset($messages))
    @unless($messages->isEmpty())
        <script>
            function hideNotification(id) {
                document.cookie = "hide-notification-" + id + "=true; expires={{ now()->addYears(10)->toCookieString() }}";
            }
        </script>
        @foreach ($messages as $message)
            @unless (request()->hasCookie('hide-notification-' . $message->id))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message->content }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="hideNotification({{ $message->id }})">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    @moderator
                        <hr>
                        <p class="mb-0">
                            <b>Zpráva #{{ $message->id }}</b>
                            &mdash;
                            <small class="uk-text-meta">Přidal {{ $message->author->username }}, expiruje {{ $message->expiration }}</small>
                            &mdash;
                            <small>Tuto informaci vidí pouze moderátor</small>
                        </p>
                    @endmoderator
                </div>
            @endif
        @endforeach
    @endunless
@endif
