@extends('layout.master')

@section('nav')
    @breadcrumbs([
        'Předměty' => null,
    ])
@endsection

@section('content')

    @include('includes.messages')

    <div class="row">
        <div class="col-sm-12">
            <h3>Předměty</h3>
            <div class="list-group">
                @foreach($subjects as $subject)
                    <a href="{{ route('subjects.show', $subject->id) }}" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                        <b>{{ $subject->name }}</b>
                        <small>Obsahuje {{ count($subject->tasks) }} úloh</small>
                    </a>
                @endforeach
                @moderator
                    <a href="{{ route('subjects.create') }}" class="list-group-item list-group-item-action text-light bg-success">
                        Přidat předmět
                    </a>
                @endmoderator
            </div>
        </div>
        <div class="col-sm-12 mt-4">
            <hr>
            <h3>Nejnovější testy</h3>
            <ul class="list-group">
                @foreach ($testcases as $testcase)
                    <div class="list-group-item">
                        <b>{{ $testcase->author->username }}</b>
                        nahrál/a test
                        <a href="{{ route('testcases.show', $testcase->id) }}">#{{ str_pad($testcase->id, 5, '0', STR_PAD_LEFT) }}</a>
                        k úloze
                        <a href="{{ route('tasks.show', $testcase->task->id) }}">{{ $testcase->task->name }}</a>
                        v předmětu
                        <a href="{{ route('subjects.show', $testcase->task->subject->id) }}">{{ $testcase->task->subject->name }}</a>
                        {{ (new \Carbon\Carbon($testcase->created_at))->diffForHumans() }}
                    </div>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-12 mt-4">
            <hr>
            <h3>Nejnovější asserty</h3>
            <ul class="list-group">
                @foreach ($asserts as $assert)
                    <div class="list-group-item">
                        <b>{{ $assert->author->username }}</b>
                        nahrál/a assert
                        <a href="{{ route('asserts.show', $assert->id) }}">#{{ str_pad($assert->id, 5, '0', STR_PAD_LEFT) }}</a>
                        k úloze
                        <a href="{{ route('tasks.show', $assert->task->id) }}">{{ $assert->task->name }}</a>
                        v předmětu
                        <a href="{{ route('subjects.show', $assert->task->subject->id) }}">{{ $assert->task->subject->name }}</a>
                        {{ (new \Carbon\Carbon($assert->created_at))->diffForHumans() }}
                    </div>
                @endforeach
            </ul>
        </div>
    </div>

@endsection
