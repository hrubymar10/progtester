@extends('layout.master')

@section('nav')
    @breadcrumbs([
        'Předměty' => route('index'),
        'Přidat předmět' => null
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Přidat předmět</h1>
            <form action="{{ route('subjects.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Název předmětu</label>
                    <input class="form-control" id="name" type="text" name="name" required>
                </div>
                <button class="btn btn-primary">Přidat</button>
            </form>
        </div>
    </div>
@endsection