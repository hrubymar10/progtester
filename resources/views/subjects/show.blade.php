@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Předměty' => route('index'),
            $subject->name => null
        ])
    !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h3>{{ $subject->name }}</h3>
            <div class="list-group">
                @foreach ($subject->tasks->reverse() as $task)
                    @if ($task->archived)
                        <div class="list-group-item disabled d-flex justify-content-between align-item-center">
                            <del>{{ $task->name }}</del>
                            <small>Úloha byla archivována moderátorem</small>
                        </div>
                    @else
                        <a href="{{ route('tasks.show', $task->id) }}" class="list-group-item list-group-item-action d-flex justify-content-between align-item-center">
                            <b>{{ $task->name }}</b>
                            <small>Obsahuje {{ count($task->testcases) }} testů a {{ count($task->assertTestcases) }} assertů</small>
                        </a>
                    @endif
                @endforeach
                @moderator
                    <a href="{{ route('tasks.create', $subject->id) }}" class="list-group-item list-group-item-action d-flex justify-content-between align-item-center text-light bg-success">
                        Přidat úlohu
                    </a>
                @endmoderator
            </div>
        </div>
    </div>
@endsection
