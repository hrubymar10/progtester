<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>503 &mdash; Down for maintenance</title>
    <style>
        body { background: #828273; }
        main { text-align: center; }
        main h1 { font-size: 50px; }
    </style>
</head>
<body>
    <main>
        <img src="{{ asset('img/503.png') }}" alt="">
        <h1>503</h1> 
        <h2>Momentálně probíhá údržba systému.</h2>
        <p>Nemělo by to trvat déle než 15 minut.</p>
    </main>
</body>
</html>