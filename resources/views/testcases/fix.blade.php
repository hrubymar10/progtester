@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Předměty' => route('index'),
            $testcase->task->subject->name => route('subjects.show', $testcase->task->subject->id),
            $testcase->task->name => route('tasks.show', $testcase->task->id),
            'Test #' . $testcase->id => route('testcases.show', $testcase->id),
            'Nahrát lepší výstup' => null
        ])
    !!}
@endsection

@section('content')
    <h1>Nahrát lepší výstup k testu #{{ $testcase->id }}</h1>

    <form action="{{ route('full-output.store', $testcase->id) }}" method="post">
        @csrf
        <div class="form-group">
            <label for="output-data">Výstupní data:</label>
            <textarea name="output-data" id="output-data" class="form-control" required style="min-height: 200px;"></textarea>
        </div>
        <button class="btn btn-success">Nahrát</button>
    </form>
@endsection
