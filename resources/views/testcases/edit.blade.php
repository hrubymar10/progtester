@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Předměty' => route('index'),
            $task->subject->name => route('subjects.show', $task->subject->id),
            $task->name => route('tasks.show', $task->id),
            'Upravit test #' . $testcase->id => null
        ])
    !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Upravit test #{{ $testcase->id }}</h1>
        </div>

        <div class="col-sm-12">
            <form action="{{ route('testcases.update', $testcase->id) }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Poznámka (popis) k testu <small class="text-muted">&mdash; Nepovinné</small></label>
                            <input type="text" class="form-control" name="description" id="description" value="{{ $testcase->description }}">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="input-data">Vstupní data</label>
                            <textarea name="input-data" rows="10" id="input-data" class="form-control">{{ $testcase->input_data }}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <label for="output-data">Očekávaný výstup</label>
                        <textarea name="output-data" rows="10" id="output-data" class="form-control" required>{{ $testcase->output_data }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="bonus" name="bonus" {{ $testcase->bonus ? 'checked' : '' }}>
                        <label class="custom-control-label" for="bonus">Bonusová otázka?</label>
                    </div>
                </div>
                <button class="btn btn-primary" onclick="this.disabled = true; this.form.submit()">Uložit</button>
            </form>
        </div>
    </div>
@endsection
