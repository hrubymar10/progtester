@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Předměty' => route('index'),
            $testcase->task->subject->name => route('subjects.show', $testcase->task->subject->id),
            $testcase->task->name => route('tasks.show', $testcase->task->id),
            'Test #' . $testcase->id => null
        ])
    !!}
@endsection

@section('content')
    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-12">
            <div class="d-flex align-items-center">
                @unless($previous == null)
                    <a href="{{ route('testcases.show', $previous->id) }}" class="mr-2">Předchozí</a>
                @endunless
                    <h1>Test #{{ $testcase->id }}</h1>
                @unless($next == null)
                    <a href="{{ route('testcases.show', $next->id) }}" class="ml-2">Další</a>
                @endunless
            </div>
            <div class="text-{{ $testcase->archive ? 'muted' : 'danger' }}">
                {{ $testcase->approvals_count }} &times; Potvrzeno,
                {{ $testcase->flags_count }} &times; Nahlášeno jako nefunkční
            </div>
            <div class="d-flex justift-content-start align-items-center py-3">
                <small>
                    {{ $testcase->task->name }} &mdash; {{ $testcase->created_at }}
                </small>
                @if ($testcase->bonus)
                    <div class="badge badge-warning ml-2">
                        Test k bonusové otázce
                    </div>
                @endif
                @if ($testcase->editable)
                    <div class="badge badge-info ml-2">
                        Potřeboval by lepší výstup
                    </div>
                @endif
            </div>
        </div>
        @if ($testcase->description !== null)
            <div class="col-sm-12">
                <div class="alert alert-info">{{ $testcase->description }}</div>
            </div>
        @endif
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label for="input_data">Vstupní data</label>
                <textarea class="form-control" id="input_data" style="font-family: monospace; min-height: 200px;">{{ $testcase->input_data }}</textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-light" onclick="javascript:copy('input_data', this)">Kopírovat</button>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label for="output_data">Očekávaný výstup</label>
                <textarea class="form-control" id="output_data" style="font-family: monospace; min-height: 200px;">{{ $testcase->output_data }}</textarea>
                @if ($testcase->editable)
                    <div class="alert alert-info mt-3">
                        <p>
                            Tento test by potřeboval lepší výstup. <br>
                            Vypadá to že ProgTest nebyl v tomto případě příliš sdílný.
                        </p>
                        <p>
                            Pokud máte funkční program, můžete <a href="{{ route('full-output.create', $testcase->id) }}" class="alert-link">nahrát lepší výstup</a>
                        </p>
                    </div>
                    @foreach($testcase->fixes as $fix)
                        <div class="alert alert-success mt-3">
                            <b>{{ $fix->author->username }}</b> nahrál lepší výstup: <br>
                            <textarea rows="10" class="form-control">{{ $fix->output_data }}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-light" onclick="javascript:copy('output_data', this)">Kopírovat</button>
            </div>
        </div>
        <div class="mb-3 col-sm-12 d-flex justify-content-end">
            @can('edit', $testcase)
                <a class="btn btn-secondary" href="{{ route('testcases.edit', $testcase->id) }}">Upravit</a>
            @endcan
            @can('destroy', $testcase)
                <form action="{{ route('testcases.destroy', $testcase->id) }}" style="display: inline-block;" method="post">
                    @method('delete')
                    @csrf
                    <button class="btn btn-danger ml-2" onclick="return confirm('Opravdu smazat test?');">Smazat</button>
                </form>
            @endcan
        </div>
            <script>
                function copy (id, button)
                {
                    let source = document.getElementById(id);

                    if (typeof source !== 'undefined')
                    {
                            const el = document.createElement('textarea');
                                el.value = source.value;

                            document.body.appendChild(el);
                            el.select();

                            document.execCommand('copy');
                            document.body.removeChild(el);

                        button.innerText = "Zkopírováno";
                    }
                }
            </script>
            <div id="hidden-vagner"><img src="" id="hidden-vagner-img"></div>
            <style>
                #hidden-vagner {
                    position: fixed;
                    left: 0px;
                    top: calc(80% - 100px);
                    perspective: 100px;
                    transform-origin: 50% 100%;
                    transform: translateX(-200px) rotateY(90deg) rotate(90deg);
                    transition: 0.3s all;
                    height: 200px;
                }
                #hidden-vagner.shown {
                    transform: translateX(-100px) rotateY(0deg) rotate(90deg);
                }
                #hidden-vagner-img {
                    height: 200px;
                }
            </style>
            <script>
                const yes = "{{ asset('img/yes.png') }}"
                const nope = "{{ asset('img/nope.png') }}"
                const vagner = document.getElementById("hidden-vagner");
                const img = document.getElementById("hidden-vagner-img");
            </script>
            <div class="col-sm-12">
                <div>
                    @can('approve', $testcase)
                        <a href="{{ route('testcases.approve', $testcase->id) }}" class="btn btn-success mb-3" id="approve">Potvrdit správnost testu</a>
                        <script>
                            document.getElementById('approve').addEventListener('mouseover', function () { vagner.classList.add('shown'); img.setAttribute('src', yes); });
                            document.getElementById('approve').addEventListener('mouseout', function () { vagner.classList.remove('shown'); });
                        </script>
                    @endcan
                    @can('flag', $testcase)
                        <a href="{{ route('testcases.flag', $testcase->id) }}" class="btn btn-danger mb-3" id="flag">Nahlásit jako nefunkční</a>
                        <script>
                            document.getElementById('flag').addEventListener('mouseover', function () { vagner.classList.add('shown'); img.setAttribute('src', nope); });
                            document.getElementById('flag').addEventListener('mouseout', function () { vagner.classList.remove('shown'); });
                        </script>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
