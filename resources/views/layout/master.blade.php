<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ProgTester &mdash; Komunitní databáze testů k ProgTest úlohám</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @if (request()->hasCookie('dark-theme'))
        <link rel="stylesheet" href="https://bootswatch.com/4/slate/bootstrap.min.css">
    @else
        <link rel="stylesheet" href="https://bootswatch.com/4/lumen/bootstrap.min.css">
    @endif
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/js/mdb.min.js"></script>
    @stack('links')
    @include('includes.analytics')
</head>
<body>
    <header @unless($errors->any()) class="uk-margin-bottom" @endif>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ route('index') }}">ProgTester</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('blog.index') }}">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://forms.gle/uVkwYURKbpM5GtNB7" target="_blank">Zpětná vazba</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="toggleTheme()">{{ request()->hasCookie('dark-theme') ? 'Day mode' : 'Night mode' }}</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    @auth
                        <li class="nav-item">
                            <a class="nav-link active" href="#" tabindex="-1">
                                {{ auth()->user()->username }}
                                @moderator
                                    <span class="badge badge-danger ml-2">Moderátor</span>
                                @endmoderator
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" class="nav-link">Logout</a>
                        </li>
                    @endauth
                </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @yield('nav')
            </ol>
        </nav>
    </header>
    <main>
        @if ($errors->any())
            <div class="container">
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first() }}
                </div>
            </div>
        @endif

        <div class="container">
            @yield('content')
        </div>

        @moderator
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 my-5 py-2">
                        <h4>Správa progtesteru</h4>
                        <a href="{{ route('users.index') }}" class="btn btn-light">Uživatelé</a>
                        <a href="{{ route('messages.index') }}" class="btn btn-light">Zprávy</a>
                        <a href="{{ route('blogposts.index') }}" class="btn btn-light">Příspěvky (blog)</a>
                    </div>
                </div>
            </div>
        @endmoderator
    </main>
    <footer class="py-3 mt-3">
        <div class="container">

            <div class="d-flex justify-content-between text-muted">
                <small class="text-muted">ProgTester verze {{ \Tremby\LaravelGitVersion\GitVersionHelper::getVersion() }}</small>
                <small>
                    &copy; Jiří Vrba, zdrojový kód a hlášení chyb a návrhů na <a class="uk-link" href="https://www.github.com/jirkavrba/progtester" target="_blank">GitHubu</a>
                </small>
            </div>
        </div>
    </footer>
    <script>
        function toggleTheme()
        {
            if (typeof Cookies.get('dark-theme') == 'undefined')
            {
                Cookies.set('dark-theme', true, { expires: 2147483647 });
            }
            else
            {
                Cookies.remove('dark-theme');
            }

            window.location.reload();
        }
    </script>
</body>
</html>
