@extends('layout.master')

@section('nav')
    {!! 
        breadcrumbs([
            'Předměty' => route('index'),
            $subject->name => route('subjects.show', $subject->id),
            'Přidat novou úlohu' => null
        ])
    !!}  
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="uk-heading-small">Přidat novou úlohu</h1>
        </div>
        <div class="col-sm-12">
            <form action="{{ route('tasks.store', $subject->id) }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Název úlohy</label>
                    <input class="form-control" id="name" type="text" name="name" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Přidat</button>
                </div>
            </form>
        </h1>
    </div>
@endsection