@extends('layout.master')

@section('nav')
    {!!
        breadcrumbs([
            'Předměty' => route('index'),
            $task->subject->name => route('subjects.show', $task->subject->id),
            $task->name => null
        ])
    !!}
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h3>{{ $task->name }}</h3>
            <div class="my-3">
                <a href="{{ route('testcases.create', $task->id) }}" class="btn btn-secondary">Přidat test</a>
                <a href="{{ route('asserts.create', $task->id) }}" class="btn btn-secondary ml-2">Přidat assert</a>
                @moderator
                    <script>
                        function confirmArchivation()
                        {
                            return confirm("Smazat úlohu? Tato operace může mít katastrofální následky?") &&
                                   confirm("Opravdu archivovat?") &&
                                   confirm("OPRAVDU ARCHIVOVAT???");
                        }
                    </script>
                    <a href="{{ route('tasks.archive', $task->id) }}" class="btn btn-danger ml-2" onclick="return confirmArchivation()">Archivovat</a>
                @endmoderator
            </div>
        </div>
        <div class="col-sm-12">
                @unless ($task->testcases->isEmpty())
                    <div class="mt-3">
                        <form action="{{ route('tasks.download', $task->id) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="include-bonus" name="include-bonus" checked>
                                    <label class="custom-control-label" for="include-bonus">
                                        Zahrnout i testy označené jako <span class="badge badge-warning">BONUS</span>
                                    </label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="include-invalid" name="include-invalid">
                                    <label class="custom-control-label" for="include-invalid">Zahrnout i testy, které byly automaticky vyřazeny (ve výpisu označeny červeně)</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <select name="line-endings" id="line-endings" class="form-control">
                                    <option value="lf">LF</option>
                                    <option value="crlf">CRLF</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Stáhnout testy jako archiv</button>
                            </div>
                        </form>
                    </div>
                @endif
                @unless ($task->assertTestcases->isEmpty())
                    <a class="btn btn-primary mb-3" href="{{ route('tasks.combine', $task->id) }}">Zkombinovat všechny asserty do jednoho kusu kódu</a>
                @endunless
            <hr>
            <h3>Klasické testy <span class="text-muted">(in.txt a out.txt)</span></h3>
            <div class="list-group mb-3">
                @forelse($task->testcases->sortBy('archive')->reverse() as $testcase)
                    <a class="list-group-item list-group-item-action @unless($testcase->archive) list-group-item-danger @endunless d-flex justify-content-between align-items-center" href="{{ route('testcases.show', $testcase->id) }}">
                        <div>
                            <b>#{{ str_pad($testcase->id, 5, '0', STR_PAD_LEFT) }}</b> &mdash;
                            {{ $testcase->author->username }}
                            @if($testcase->editable) <span class="badge badge-info">Potřeboval by lepší výstup</span> @endif
                            @unless ($testcase->archive) &mdash; Nebude zahrnuto do archivu @endif
                        </div>
                        <div class="d-flex align-items-center">
                            <div>
                                @if ($testcase->bonus) <b class="badge badge-warning mr-2">BONUS</b> @endif
                                <b title="Potvrzena správnost">{{ $testcase->approvals_count }} &#10003;</b>
                                <b title="Nahlášeno jako nefunkční">{{ $testcase->flags_count }} &times; </b>
                            </div>
                            <div class="progress ml-3" style="width: 100px">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="flex: {{ $testcase->approvals_count / max($testcase->approvals_count + $testcase->flags_count, 1) }}"></div>
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" style="flex: {{ $testcase->flags_count / max($testcase->approvals_count + $testcase->flags_count, 1) }}"></div>
                            </div>
                        </div>
                    </a>
                @empty
                    <p>Zatím nikdo nenahrál žádný test</p>
                @endforelse
            </div>
            <hr>
            <h3>Assert testy <span class="text-muted">(kousky kódy)</span></h3>
            <div class="list-group">
                @forelse($task->assertTestcases->sortBy('archive')->reverse() as $testcase)
                    <a class="list-group-item list-group-item-action @unless($testcase->archive) list-group-item-danger @endunless d-flex justify-content-between align-items-center" href="{{ route('asserts.show', $testcase->id) }}">
                        <div>
                            <b>#{{ str_pad($testcase->id, 5, '0', STR_PAD_LEFT) }}</b> &mdash;
                            {{ $testcase->author->username }}
                            @unless ($testcase->archive) &mdash; Nebude zahrnuto do archivu @endif
                        </div>
                        <div>
                            @if ($testcase->bonus) <b class="badge badge-warning mr-2">BONUS</b> @endif
                            <b title="Potvrzena správnost">{{ $testcase->approvals_count }} &#10003;</b>
                            <b title="Nahlášeno jako nefunkční">{{ $testcase->flags_count }} &times; </b>
                        </div>
                        <div class="progress ml-3" style="width: 100px">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="flex: {{ $testcase->approvals_count / max($testcase->approvals_count + $testcase->flags_count, 1) }}"></div>
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" style="flex: {{ $testcase->flags_count / max($testcase->approvals_count + $testcase->flags_count, 1) }}"></div>
                        </div>
                    </a>
                @empty
                    <p>Zatím nikdo nenahrál žádný assert</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
