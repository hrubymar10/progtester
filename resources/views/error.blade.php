@extends('layout.master')

@section('nav')
    {!! 
        breadcrumbs([
            'Error' => null
        ])
    !!}
@endsection

@section('content')
    <div class="title error">
        Něco se nepovedlo
    </div>
    <div class="cell">
        <p>
            Nečtu error logy tak často, kdyžtak mi pošli mail na <a href="mailto:jiri@vrba.dev">jiri@vrba.dev</a>.
        </p> 
    </div>
@endsection