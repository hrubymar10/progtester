@php $uuid = strtoupper(substr(hash('crc32', uniqid()), 4)); @endphp

/*
 * K zapnuti testovani automaticky vyrazenych assertu,
 * nastav nasledujici definici na 1.
 */
#define INVALID_{{ $uuid }} 0


/* 
 * K vypnuti testovani assertu oznacenych jako bonus,
 * nastav nasledujici definici na 0.
 */
#define BONUS_{{ $uuid }} 1

@foreach ($asserts as $assert)
/**
* Assert #{{ $assert->id }}
* @author {{ $assert->author->username }}
* @link {{ route('asserts.show', $assert->id) }}
* 
* Potvrzena spravnost: {{ $assert->approvals_count }}x
* Nahlasena nefukcnost: {{ $assert->flags_count }}x
* Bonus: {{ $assert->bonus ? 'ANO' : 'NE' }}
*
*/
@unless ($assert->archive)
if (INVALID_{{ $uuid }}) 
{
@endunless
@if ($assert->bonus)
if (BONUS_{{ $uuid }}) 
{
@endif
{ 
    {{ trim($assert->content) }} 
}
@if ($assert->bonus)
}
@endif
@unless ($assert->archive)
}
@endunless
@endforeach